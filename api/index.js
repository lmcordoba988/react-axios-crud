import express from "express";
import mongoose from "mongoose";
import cors from "cors";

import posts from "./routes/posts.js";
const app = express();
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/api/posts", posts);

mongoose
  .set({ strictQuery: false })
  .connect(`mongodb://mateocordobaSD:B0undm4n@ac-stsdssx-shard-00-00.olnhimn.mongodb.net:27017,ac-stsdssx-shard-00-01.olnhimn.mongodb.net:27017,ac-stsdssx-shard-00-02.olnhimn.mongodb.net:27017/HOSPITAL_DB?ssl=true&replicaSet=atlas-oj3p0x-shard-0&authSource=admin&retryWrites=true&w=majority`)
  .then(() => console.log("Connected to MongoDB..."))
  .catch((err) => console.error(`Could not connected to MongoDB... ${err}`));

const port = process.env.PORT || 9000;

app.listen(port, () => console.log(`Listening on port ${port}`));
