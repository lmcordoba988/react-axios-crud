import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";

import axios from "axios";
import config from '../../config.json';


import './Post.css';

export default function Post(){
    const navigate = useNavigate();
    const { id } = useParams();
    console.log('====================================');
    console.log(id);
    console.log('====================================');


    const [post, setPost] = useState({
        title:"",
        content:""
    });

    useEffect(()=>{
        if(id === 'new') return;
        const fetchPost = async ()=> {
            const {data} = await axios.get(`${config.api_url}/${id}`)
            setPost(data)
        }
        fetchPost()

        
    },[])


    const inputChange = (e) => {
        const postAux = {...post};
        postAux[e.target.name] = e.target.value;
        setPost(postAux);
        console.log('====================================');
        console.log(postAux);
        console.log('====================================');

    }

    const submit = async (e) => {
        e.preventDefault();
        try {
            
            if(id === 'new'){
    
                await axios.post(`${config.api_url}`,post)
                return navigate('/')
            }else{
                await axios.put(`${config.api_url}/${id}`,post);
                return navigate('/')
            }
        } catch (error) {
            console.log('====================================');
            console.log(error);
            console.log('====================================');
            
        }



    }
    
    console.log('====================================');
    console.log(post);
    console.log('====================================');




    return (
        <>
            <div className="container">
            {/* <h1 className="text-center"> {post.title === 'title...'? 'Nuevo Post': post.title}</h1> */}
                    <form className="post">
                        {/* <label> Título</label> */}
                        <input name="title"placeholder='Título...' onChange={inputChange} type="text" value={post.title} />
                        {/* <label> Contenido</label> */}
                        <input name="content" placeholder='Contenido...' onChange={inputChange} type="text" value={post.content} />
                        
                        <button onClick={submit} className="btn btn-primary btn-block">{id === 'new'? 'Publicar':'Actualizar'}</button>
                    </form>
                    
            </div>

        </>
    )
} 