import { useEffect,useState } from 'react';
import axios from 'axios';
import './Posts.css';
import config from '../../config.json'
import { useNavigate } from 'react-router-dom';





const Posts = () => {
    
    const navigate = useNavigate();
    const [posts, setPosts] = useState([]);

    const deletePost = async(postId) =>{
        try {
            setPosts(posts.filter(p => p._id !== postId))
            await axios.delete(`${config.api_url}/${postId}`);
        } catch (error) {
            
        }

        

        
    }


    useEffect(()=>{
        const fetchPosts = async ()=>{
            const {data} = await axios.get(config.api_url);
            setPosts(data)

        };
        fetchPosts();
    },[])

    console.log('====================================');
    console.log('POSTS');
    console.log('====================================');

    return (
        <div className='posts'>
            <div className='container'>
            <h1 className='text-center'>POSTS</h1>

                <button onClick={() => navigate("/post/new")} className="btn btn-primary text-align-start mb-3">Nuevo post</button>     
                <table className="table">
                    <thead>
                        <tr>
                            <th>Titulo</th>
                            <th>Texto</th>
                            <th>Actualizar</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        {posts.map( (post) => (
                            <tr key={post._id}>
                                <td>{post.title}</td>
                                <td>{post.content}</td>
                                <td><button onClick={()=> navigate(`/post/${post._id}`)}  className="btn btn-primary">Update</button></td>
                                <td><button onClick={()=> deletePost(post._id)}  className="btn btn-danger">Delete</button></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                    
                    
                </div>

        </div>

    )
}


export default Posts