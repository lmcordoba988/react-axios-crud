import Posts from './Pages/Posts/Posts';
import Post  from './Pages/Post/Post'
import './App.css';

import {Route, Routes} from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <header className="App-header">

        <Routes>
          <Route path='/' element={<Posts />} ></Route>
          <Route path='/post/:id' element={<Post />}></Route>

        </Routes>

        
        
          

      </header>
    </div>
  );
}

export default App;
